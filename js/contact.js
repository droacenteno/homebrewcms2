$('#contactform').validate( {
	rules : {
		Name_first : {
			required : true,
			minlength : 2
		},
		Name_middle : {
			required : false
		},
		Name_last : {
			required : true,
			minlength : 2
		},
		email : {
			required : true,
			email : true
		},
		Telephone : {
			phoneNL : true
		},
		Address_street : {
			required : false
		},
		Address_number : {
			required : false
		},
		Address_zipcode : {
			required : false,
			postalcodeNL : true
		},
		Address_city : {
			required : false
		},
		Subject : {
			required : true,
			minlength : 10
		},
		Message : {
			required : true,
			minlength : 10
		}
	},
	highlight : function(element) {
		$(element).closest('.form-group').addClass('has-error');
	},
	unhighlight : function(element) {
		$(element).closest('.form-group').removeClass('has-error');
	},
	errorElement : 'span',
	errorClass : 'help-block',
	errorPlacement : function(error, element) {
		if (element.parent('.input-group').length) {
			error.insertAfter(element.parent());
		} else {
			error.insertAfter(element);
		}
	}
});


